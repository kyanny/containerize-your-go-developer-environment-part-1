all: bin/example

PLATFORM=local

.PHONY: bin/example clean

bin/example:
	@docker build . --target bin \
	--output bin/ \
	--platform ${PLATFORM}

clean:
	@rm bin/*